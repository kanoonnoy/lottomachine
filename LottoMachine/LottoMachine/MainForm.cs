﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LottoMachine
{
    public partial class MainForm : Form
    {
        private int childFormNumber = 0;
        private Base.Class.TableEntity table = new Base.Class.TableEntity();
        private Base.Forms.ChildForm childform;


        #region Constructor
        public MainForm()
        {
            InitializeComponent();
        }
        #endregion
        

        #region Reload
        private void Reload()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;


                //var Provider = "Microsoft.Jet.OLEDB.4.0";
                //if (Environment.Is64BitOperatingSystem)
                //{
                //    Provider = "Microsoft.ACE.OLEDB.12.0";
                //}
                //Properties.Settings.Default["RealtimeConnectionString"] = string.Format("Provider={0};Data Source={1}\\Realtime.mdb", Provider,Global.CurrentRootPath);
                //Properties.Settings.Default.Save();
                //var aftersave = Properties.Settings.Default["RealtimeConnectionString"].ToString();
                //Console.WriteLine(aftersave);

                var dlg = new SelectLottoDateForm();
                if(dlg.ShowDialog() != DialogResult.Yes)
                {
                    this.Close();
                    return;

                }

                this.WindowState = FormWindowState.Maximized;


                bool isDemo = false;
                var text = isDemo ? "เวอร์ชั่นทดลองใช้งาน" : "ลงทะเบียนการใช้งานเรียบร้อยแล้ว";
                this.toolStripStatusLabel.Text = string.Format("เข้าสู่ระบบโดย : {0} เมื่อ {1:dd/MM/yyyy HH:mm:ss} {2}" ,Global.CurrentUsername,DateTime.Now,text);
                
                // Show 
                OpenForm(new Lotto.LottoForm());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            Reload();
            this.toolStripLabel1.Text = string.Format("งวดประจำวันที่ : {0:dd MMMM yyyy}", Global.CurrentLottoDate.Date);

        }
        #endregion

        #region 1. Menu Start

        private void ShowNewForm(object sender, EventArgs e)
        {
            OpenForm(new Lotto.LottoForm());
        }
        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenForm(new Report.DynamicReportForm());
        }

        // Open form
        private void OpenForm(Form newForm)
        {
            if (!CheckForDuplicate(newForm))
            {
                newForm.MdiParent = this;
                newForm.Show();
                newForm.WindowState = FormWindowState.Maximized;
                newForm.Activate();
            }
            else
            {
                newForm.Dispose();
            }
        }
        private bool CheckForDuplicate(Form newForm)
        {
            bool bValue = false;
            foreach (Form fm in this.MdiChildren)
            {
                if (fm.GetType() == newForm.GetType())
                {
                    fm.Activate();
                    fm.WindowState = FormWindowState.Maximized;
                    bValue = true;
                }
            }
            return bValue;
        }

        #endregion



        #region Events Menu
        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }
        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }
        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }
        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }
        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }
        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }
        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAll();
        }
        private void btCreateLotto_Click(object sender, EventArgs e)
        {

        }
        private void CloseAll()
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }
        private void ClearAll()
        {
            try
            {
                if (MessageBox.Show(string.Format("คุณต้องการล้างข้อมูลหวยงวดประจำวันที่ '{0:dd MMMM yyyy}' ทั้งหมดหรือไม่?", Global.CurrentLottoDate), "ล้างข้อมูลหวย", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                {
                    return;
                }
                
                using (LOTTOEntities lotto = new LOTTOEntities())
                {
                    lotto.Database.ExecuteSqlCommand("TRUNCATE TABLE [LottoNumber]");
                }

                MessageBox.Show(string.Format("ล้างข้อมูลงวดวันที่ '{0:dd MMMM yyyy}' เรียบร้อยแล้ว", Global.CurrentLottoDate), "ล้างข้อมูลประจำงวด", MessageBoxButtons.OK, MessageBoxIcon.Information);
                CloseAll();
                OpenForm(new Lotto.LottoForm());


                //var str = string.Format("DELETE FROM LottoNumber WHERE LottoDate = #{0}# ",Global.CurrentLottoDate.ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US")));
                //table.Source = Base.Class.TableEntity.Database.LigorLotto;
                //if(table.SQLNoneOleQuery(str))
                //{
                //    MessageBox.Show(string.Format("ล้างข้อมูลงวดวันที่ '{0:dd MMMM yyyy}' เรียบร้อยแล้ว", Global.CurrentLottoDate), "ล้างข้อมูลประจำงวด", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    CloseAll();
                //    OpenForm(new Lotto.LottoForm());
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void ลางขอมลทงหมดToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
                ClearAll();
            
        }
        

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // ตั้งค่าเลขอั้น
            var dlg = new Lotto.LottoLimitedForm();
            var result = dlg.ShowDialog();
            if(result == DialogResult.Yes)
            {
                this.CloseAll();
                OpenForm(new Lotto.LottoForm());
            }
        }
        private void เปลยนรหสผานToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // เปลี่ยนรหัสผ่าน
            //var dlg = new Account.AccountManageForm();
            //dlg.ShowDialog();
        }
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // เกี่ยวกับโปรแกรม
            var dlg = new About.AboutForm();
            dlg.ShowDialog();
        }

        #endregion
    }
}
