﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LottoMachine
{
    public partial class LoginForm : Form
    {
        private Base.Class.TableEntity table = new Base.Class.TableEntity();

        public LoginForm()
        {
            InitializeComponent();
        }

        private void Reload()
        {
            try
            {
                //var Provider = "Microsoft.Jet.OLEDB.4.0";
                //if (Environment.Is64BitOperatingSystem)
                //{
                //    Provider = "Microsoft.ACE.OLEDB.12.0";
                //}
                //Properties.Settings.Default["RealtimeConnectionString"] = string.Format("Provider={0};Data Source={1}\\Realtime.mdb", Provider, Global.CurrentRootPath);
                //Properties.Settings.Default.Save();
                //var aftersave = Properties.Settings.Default["RealtimeConnectionString"].ToString();
                //Console.WriteLine(aftersave);

                this.textBox1.Text = Global.CurrentUsername;
                
                this.textBox2.Text = "";
                this.textBox2.Select();
                this.textBox2.Focus();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show(ex.Message);
            }
        }
        private void LoginForm_Load(object sender, EventArgs e)
        {
            Reload();
        }

        private void Login()
        {
            try
            {
                this.btConfrim.Enabled = false;
                this.Cursor = Cursors.WaitCursor;

                if(this.textBox2.TextLength == 0 || this.textBox1.TextLength == 0)
                {
                    return;
                }
                var isSuccess = false;
                var username = this.textBox1.Text;
                var password = this.textBox2.Text;

                using (LOTTOEntities lotto = new LOTTOEntities())
                {
                    var query = from db in lotto.UserAccount
                                where db.Password == password
                                select db;
                    if(query !=null && query.Count() > 0)
                    {
                        isSuccess = true;
                    }
                }


                //var str = string.Format("SELECT * FROM Account WHERE Password = '{0}'", password);
                //table.Source = Base.Class.TableEntity.Database.LigorLotto;
                //if(table.SQLQuery(str))
                //{
                //    if(table.Records.Rows.Count > 0)
                //    {
                //        isSuccess = true;
                //    }
                //}
                if(!isSuccess)
                {
                    MessageBox.Show("ขออภัย! ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้องค่ะ","เข้าสู่ระบบ",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                    return;
                }

               

                this.Hide();
                var dlg = new MainForm();
                dlg.ShowDialog();
                this.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.btConfrim.Enabled = true;
                this.Cursor = Cursors.Default;
            }
        }
        private void btConfrim_Click(object sender, EventArgs e)
        {
            Login();
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyData == Keys.Enter)
            {
                Login();
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.ligorsolution.com/about");
            
        }

        private void btExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
