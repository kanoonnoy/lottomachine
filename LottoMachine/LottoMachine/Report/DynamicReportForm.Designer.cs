﻿namespace LottoMachine.Report
{
    partial class DynamicReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.reportViewer1 = new LottoMachine.Base.Control.ReportViewer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btPrint = new System.Windows.Forms.Button();
            this.btRelink = new System.Windows.Forms.Button();
            this.PrintCountComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.PrintTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lottoNumberTableAdapter1 = new LottoMachine.RealtimeDataSetTableAdapters.LottoNumberTableAdapter();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportViewer1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.reportViewer1.Location = new System.Drawing.Point(0, 67);
            this.reportViewer1.Margin = new System.Windows.Forms.Padding(4);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ReportNumber = "";
            this.reportViewer1.SetDataBasePath = "D:\\9001 Ligor Solution Group\\Root\\LottoMachine\\Realtime.mdb";
            this.reportViewer1.SetReportPath = "D:\\9001 Ligor Solution Group\\Root\\LottoMachine\\REPORT";
            this.reportViewer1.Size = new System.Drawing.Size(845, 577);
            this.reportViewer1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.btPrint);
            this.groupBox1.Controls.Add(this.btRelink);
            this.groupBox1.Controls.Add(this.PrintCountComboBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.PrintTypeComboBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(845, 67);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ข้อมูลการพิมพ์";
            // 
            // button1
            // 
            this.button1.Image = global::LottoMachine.Properties.Resources.ALL_16_0013;
            this.button1.Location = new System.Drawing.Point(12, 27);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(43, 30);
            this.button1.TabIndex = 8;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btPrint
            // 
            this.btPrint.Image = global::LottoMachine.Properties.Resources.ALL_16_0014;
            this.btPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btPrint.Location = new System.Drawing.Point(512, 27);
            this.btPrint.Name = "btPrint";
            this.btPrint.Size = new System.Drawing.Size(130, 30);
            this.btPrint.TabIndex = 7;
            this.btPrint.Text = "พิมพ์(Ctrl + P)";
            this.btPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btPrint.UseVisualStyleBackColor = true;
            this.btPrint.Click += new System.EventHandler(this.btPrint_Click);
            // 
            // btRelink
            // 
            this.btRelink.Image = global::LottoMachine.Properties.Resources.ALL_16_0001;
            this.btRelink.Location = new System.Drawing.Point(463, 27);
            this.btRelink.Name = "btRelink";
            this.btRelink.Size = new System.Drawing.Size(43, 30);
            this.btRelink.TabIndex = 6;
            this.btRelink.UseVisualStyleBackColor = true;
            this.btRelink.Click += new System.EventHandler(this.btReload_Click);
            // 
            // PrintCountComboBox
            // 
            this.PrintCountComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PrintCountComboBox.FormattingEnabled = true;
            this.PrintCountComboBox.Location = new System.Drawing.Point(336, 31);
            this.PrintCountComboBox.Name = "PrintCountComboBox";
            this.PrintCountComboBox.Size = new System.Drawing.Size(121, 25);
            this.PrintCountComboBox.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(263, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "พิมพ์ครั้งที่";
            // 
            // PrintTypeComboBox
            // 
            this.PrintTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PrintTypeComboBox.FormattingEnabled = true;
            this.PrintTypeComboBox.Location = new System.Drawing.Point(136, 31);
            this.PrintTypeComboBox.Name = "PrintTypeComboBox";
            this.PrintTypeComboBox.Size = new System.Drawing.Size(121, 25);
            this.PrintTypeComboBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(61, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "ประเภทชุด";
            // 
            // lottoNumberTableAdapter1
            // 
            this.lottoNumberTableAdapter1.ClearBeforeFill = true;
            // 
            // DynamicReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(845, 644);
            this.Controls.Add(this.reportViewer1);
            this.Controls.Add(this.groupBox1);
            this.Name = "DynamicReportForm";
            this.ShowIcon = true;
            this.Text = "พิมพ์รายงานเลขรางวัล";
            this.Load += new System.EventHandler(this.DynamicReportForm_Load);
            this.Shown += new System.EventHandler(this.DynamicReportForm_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Base.Control.ReportViewer reportViewer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox PrintTypeComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox PrintCountComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btRelink;
        private System.Windows.Forms.Button btPrint;
        private RealtimeDataSetTableAdapters.LottoNumberTableAdapter lottoNumberTableAdapter1;
        private System.Windows.Forms.Button button1;
    }
}