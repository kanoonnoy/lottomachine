﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LottoMachine.Report
{
    public partial class DynamicReportForm : Base.Forms.ChildForm
    {
        #region Variable
        private bool _Istartup = true;
        private Base.Class.TableEntity table = new Base.Class.TableEntity();

        private DataTable _dtPrintCount = new DataTable();
        #endregion

        #region Constructor
        public DynamicReportForm()
        {
            InitializeComponent();
        }
        #endregion
        protected override bool ProcessCmdKey(ref Message message, Keys keys)
        {
            switch (keys)
            {
                case Keys.P | Keys.Control:
                    Print();
                    return true;
            }

            // run base implementation
            return base.ProcessCmdKey(ref message, keys);
        }
        #region Reload
        private void DynamicReportForm_Load(object sender, EventArgs e)
        {
            Reload();
        }
        public void Reload()
        {
            try
            {
                if (_Istartup)
                {
                    var dtPrintType = new DataTable();
                    dtPrintType.Columns.Add("value");
                    dtPrintType.Columns.Add("display");

                    dtPrintType.Rows.Add("1", "สำหรับเจ้ามือหวย");
                    dtPrintType.Rows.Add("2", "สำหรับส่งต่อ");

                    this.PrintTypeComboBox.DisplayMember = "display";
                    this.PrintTypeComboBox.ValueMember = "value";
                    this.PrintTypeComboBox.DataSource = dtPrintType;

                    this.PrintTypeComboBox.SelectedValue = "2";

                    _dtPrintCount.Columns.Add("value");
                    _dtPrintCount.Columns.Add("display");

                    this.PrintCountComboBox.DisplayMember = "display";
                    this.PrintCountComboBox.ValueMember = "value";
                    this.PrintCountComboBox.DataSource = _dtPrintCount;

                    _Istartup = false;
                }

                _dtPrintCount.Rows.Clear();


                using (LOTTOEntities lotto = new LOTTOEntities())
                {
                    var query = (from db in lotto.LottoNumber
                                 where db.LottoType == Constant.LOTTO_TYPE_3_NUMBER && DbFunctions.TruncateTime(db.LottoDate) == Global.CurrentLottoDate
                                 select db.PrintSequenceNumber).Distinct().ToList().OrderByDescending(x=> x.Value);

                    foreach (var print in query)
                    {
                        var value = print != null ? print : 0;
                        var item = value > 0 ? string.Format("พิมพ์ครั้งที่ {0:N0}", print) : "ยังไม่ได้พิมพ์";
                        _dtPrintCount.Rows.Add(value.ToString(), item);
                    }
                    _dtPrintCount.AcceptChanges();
                }

                //var str = string.Format("SELECT DISTINCT PrintSequenceNumber FROM LottoNumber WHERE LottoType = '{0}' AND LottoDate = #{1}# ORDER BY PrintSequenceNumber DESC", Constant.LOTTO_TYPE_3_NUMBER, Global.CurrentLottoDate.ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US")));
                //table.Source = Base.Class.TableEntity.Database.LigorLotto;
                //if (table.SQLQuery(str))
                //{
                //    if (table.Records.Rows.Count > 0)
                //    {
                //        foreach (DataRow row in table.Records.Rows)
                //        {
                //            var value = row[0] != DBNull.Value ? (int)row[0] : 0;
                //            var item = value > 0 ? string.Format("พิมพ์ครั้งที่ {0:N0}", row[0].ToString()) : "ยังไม่ได้พิมพ์";
                //            _dtPrintCount.Rows.Add(value.ToString(), item);
                //        }
                //        _dtPrintCount.AcceptChanges();
                //    }
                //}


                if (_dtPrintCount.Rows.Count > 0)
                {
                    // หากมียังชุดที่ยังไม่ได้พิมพ์ให้แสดงชุดนั้นก่อน
                    var resultSelect = _dtPrintCount.Select("value = '0'");
                    if (resultSelect.Length > 0)
                    {
                        this.PrintCountComboBox.SelectedValue = "0";
                    }
                }
                else
                {
                    //MessageBox.Show("");

                }
               


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion
        
        #region Relink
        private void Relink()
        {
            try
            {
                this.Cursor = Cursors.AppStarting;
                this.btRelink.Enabled = false;

                var printType = this.PrintTypeComboBox.SelectedValue.ToString();
                var PrintSequenceNumber = this.PrintCountComboBox.SelectedValue != null ? this.PrintCountComboBox.SelectedValue.ToString() : "0";
                var Parameter = string.Format("LottoDate=#{0}#;PrintType=\"{1}\";PrintSequenceNumber={2}",
                    Global.CurrentLottoDate.ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US")),
                   printType,
                   PrintSequenceNumber);


                Console.WriteLine(Parameter);

                this.reportViewer1.ReportNumber = "RL0001";
                this.reportViewer1.ParameterFildText = Parameter;
                //this.reportViewer1.SetDataBasePath = string.Format("{0}\\RealTime.mdb", Global.CurrentRootPath);
                this.reportViewer1.SetReportPath = string.Format("{0}\\Report", Global.CurrentRootPath);
                this.reportViewer1.Reload();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
                this.btRelink.Enabled = true;
            }
        }
        private void btReload_Click(object sender, EventArgs e)
        {
            Relink();
        }
        private void DynamicReportForm_Shown(object sender, EventArgs e)
        {
            Relink();
        }
        #endregion

        #region Print
        private void Print()
        {
            try
            {
                this.groupBox1.Enabled = false;
                this.Cursor = Cursors.WaitCursor;

                //var PrintCount = this.PrintCountComboBox.SelectedValue == null ? "0" :this.PrintCountComboBox.SelectedValue.ToString();
                //if (PrintCount != "0")
                //{
                //    return;
                //}
                var valuePrint = this.PrintCountComboBox.SelectedValue.ToString();
                if (valuePrint == "0")
                {
                    #region Print Frist
                    if (MessageBox.Show("คุณต้องการบันทึกการพิมพ์ครั้งนี้หรือไม่?", "พิมพ์", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        int LastPrintCount = 0;
                        int UpdateRowsPrint = 0;


                        using (LOTTOEntities lotto = new LOTTOEntities())
                        {
                            var query = (from db in lotto.LottoNumber
                                         orderby db.PrintSequenceNumber descending
                                         select db.PrintSequenceNumber).Distinct().OrderByDescending(x => x.Value).ToList();
                            if (query.Count > 0)
                            {
                                LastPrintCount = query[0].Value;
                                LastPrintCount++;
                            }
                        }

                        if(LastPrintCount == 0)
                        {
                            MessageBox.Show("ยังไม่มีเลขหวย");
                            return;
                        }
                        using (LOTTOEntities lotto = new LOTTOEntities())
                        {
                            var query = (from db in lotto.LottoNumber
                                         where db.LottoType == Constant.LOTTO_TYPE_3_NUMBER && DbFunctions.TruncateTime(db.LottoDate) == Global.CurrentLottoDate && db.PrintSequenceNumber == 0
                                         select db);
                            var lottonumbers = query;

                            foreach (var number in lottonumbers)
                            {
                                number.PrintSequenceNumber = LastPrintCount;
                                lotto.Entry(number).State = EntityState.Modified;
                            }

                            //var entry = lotto.Entry(lottonumbers);
                            //entry.State = EntityState.Modified;
                            UpdateRowsPrint = lotto.SaveChanges();
                            Console.WriteLine("Update {0}", UpdateRowsPrint);
                        }

                        //var strLast = "SELECT PrintSequenceNumber FROM LottoNumber ORDER BY PrintSequenceNumber DESC";
                        //table.Source = Base.Class.TableEntity.Database.LigorLotto;
                        //if (table.SQLQuery(strLast))
                        //{
                        //    if (table.Records.Rows.Count > 0)
                        //    {
                        //        LastPrintCount = table.Records.Rows[0]["PrintSequenceNumber"] == DBNull.Value ? 0 : (int)table.Records.Rows[0]["PrintSequenceNumber"];
                        //        LastPrintCount++;
                        //    }
                        //    else
                        //    {
                        //        MessageBox.Show("ยังไม่มีเลขหวย");
                        //        return;

                        //    }
                        //}
                        //var str = string.Format("SELECT * FROM LottoNumber WHERE LottoType = '{0}' AND LottoDate = #{1}# AND PrintSequenceNumber = 0", Constant.LOTTO_TYPE_3_NUMBER, Global.CurrentLottoDate.ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US")));
                        //table.Source = Base.Class.TableEntity.Database.LigorLotto;
                        //if (table.SQLQuery(str))
                        //{
                        //    if (table.Records.Rows.Count > 0)
                        //    {
                        //        var rowUpdates = table.Records;
                        //        foreach (DataRow row in rowUpdates.Rows)
                        //        {
                        //            row["PrintSequenceNumber"] = LastPrintCount;
                        //        }

                        //        UpdateRowsPrint = this.lottoNumberTableAdapter1.Update(rowUpdates.Select());

                        //    }
                        //}



                        Reload();

                        Relink();

                        this.reportViewer1.PrintReport();

                        //MessageBox.Show(string.Format("บันทึกแล้ว {0:N0} รายการ", UpdateRowsPrint));
                    }
                    #endregion

                }
                else
                {
                    this.Relink();
                    this.reportViewer1.PrintReport();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.groupBox1.Enabled = true;
                this.Cursor = Cursors.Default;
            }
        }
        private void btPrint_Click(object sender, EventArgs e)
        {
            Print();
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            Reload();
        }
    }
}
