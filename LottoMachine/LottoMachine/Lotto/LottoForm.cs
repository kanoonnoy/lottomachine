﻿using LottoMachine.Base.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LottoMachine.Lotto
{
    public partial class LottoForm : ChildForm
    {
        public LottoForm()
        {
            InitializeComponent();
        }

        private void LottoForm_Load(object sender, EventArgs e)
        {
            this.lottoPanel1.Reload();
        }
    }
}
