﻿namespace LottoMachine.Lotto
{
    partial class LottoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lottoPanel1 = new LottoMachine.Lotto.Control.LottoPanel();
            this.SuspendLayout();
            // 
            // lottoPanel1
            // 
            this.lottoPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lottoPanel1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lottoPanel1.Location = new System.Drawing.Point(0, 0);
            this.lottoPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.lottoPanel1.Name = "lottoPanel1";
            this.lottoPanel1.Size = new System.Drawing.Size(1076, 722);
            this.lottoPanel1.TabIndex = 0;
            // 
            // LottoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1076, 722);
            this.Controls.Add(this.lottoPanel1);
            this.Name = "LottoForm";
            this.ShowIcon = true;
            this.Text = "บันทึกข้อมูลเลขรางวัล";
            this.Load += new System.EventHandler(this.LottoForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Control.LottoPanel lottoPanel1;
    }
}