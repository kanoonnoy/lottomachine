﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LottoMachine.Lotto
{
    public partial class LottoLimitedForm : Form
    {
        private Base.Class.TableEntity table = new Base.Class.TableEntity();

        private decimal _CurrentMaxPrice = 0;
        private DataTable _dtLottoLimited;
        public LottoLimitedForm()
        {
            InitializeComponent();
        }
        
        private void Reload()
        {
            try
            {

                using (LOTTOEntities lotto = new LOTTOEntities())
                {
                    var query = (from db in lotto.LottoLimited
                                where db.LottoType == Constant.LOTTO_TYPE_3_NUMBER
                                select db).ToList();

                    _CurrentMaxPrice = query[0].MaxPrice != null ? query[0].MaxPrice.Value : 0;

                }
                //var str = String.Format("SELECT * FROM LottoLimited WHERE LottoType = '{0}'", Constant.LOTTO_TYPE_3_NUMBER);
                //table.Source = Base.Class.TableEntity.Database.LigorLotto;
                //if(table.SQLQuery(str))
                //{
                //    _dtLottoLimited = table.Records;
                //    if (table.Records.Rows.Count > 0)
                //    {
                //        _CurrentMaxPrice = table.Records.Rows[0]["MaxPrice"] != DBNull.Value ? (decimal)table.Records.Rows[0]["MaxPrice"] : 0;

                //    }
                //}

                this.textBox2.Text = _CurrentMaxPrice.ToString("N0");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void SetLimitedAmount()
        {
            try
            {

                var maxprice = this.textBox1.Text.ToDecimal();
                if(maxprice < 0)
                {
                    return;
                }
                var message = string.Format("คุณต้องการตั้งค่าเลขอั้นเท่ากับ {0:N0} นี้หรือไม่?", maxprice);
                if(MessageBox.Show(message, "ตั้งค่าเลขอั้น",MessageBoxButtons.YesNo,MessageBoxIcon.Question,MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    int UpdateRows = 0;
                    using (LOTTOEntities lotto = new LOTTOEntities())
                    {
                        var query = (from db in lotto.LottoLimited
                                     where db.LottoType == Constant.LOTTO_TYPE_3_NUMBER
                                     select db).ToList();

                        var lottolimited = query[0].MaxPrice = maxprice;
                        var entry = lotto.Entry(query[0]);
                        entry.State = System.Data.Entity.EntityState.Modified;

                        UpdateRows = lotto.SaveChanges();
                    }


                    //// Update 
                    //_dtLottoLimited.Rows[0]["MaxPrice"] = maxprice;
                    //_dtLottoLimited.Rows[0]["UpdateUser"] = Global.CurrentUsername;
                    //_dtLottoLimited.Rows[0]["UpdateWhen"] = DateTime.Now;

                    //int UpdateRows = this.lottoLimitedTableAdapter1.Update(_dtLottoLimited.Select());

                    if(UpdateRows > 0)
                    {
                        MessageBox.Show("บันทึกเรียบร้อยแล้ว","ตั้งค่าเลขอั้น",MessageBoxButtons.OK,MessageBoxIcon.Information);
                        this.DialogResult = DialogResult.Yes;
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyData == Keys.Enter)
            {
                SetLimitedAmount();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SetLimitedAmount();
        }

        private void LottoLimitedForm_Load(object sender, EventArgs e)
        {
            Reload();
        }
    }
}
