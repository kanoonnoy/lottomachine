﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Data.Entity;

namespace LottoMachine.Lotto.Control
{
    public partial class LottoPanel : UserControl
    {
        #region Variable
        struct LottoNumber
        {
            public string lotto;
            public decimal price;
        }
        private bool _Istartup = true;
        private DataTable _DATASHOWMASTER = new DataTable();
        private DataTable _dtDisplay = new DataTable();
        private DataTable _dtLimited = new DataTable();

        private Base.Class.TableEntity table = new Base.Class.TableEntity();

        private DateTime _CurrentDate = Global.CurrentLottoDate.Date;

        private readonly string _LottoType = Constant.LOTTO_TYPE_3_NUMBER;
        private decimal _CurrentLimited = 0;
        private decimal _CurrentTotalAll = 0;
        private decimal _CurrentTotalPrice1 = 0;
        private decimal _CurrentTotalPrice2 = 0;

        private int _LastPrintCount = 0;
        private int _CurrentNumber = 0; // All 1000 number.

        #endregion
        
        #region Constructor
        public LottoPanel()
        {
            InitializeComponent();
        }
        #endregion

        protected override bool ProcessCmdKey(ref Message message, Keys keys)
        {
            switch (keys)
            {
                case Keys.F1:
                    this.LottoTextBox1.Focus();
                    this.LottoTextBox1.SelectAll();
                    return true; // signal that we've processed this key

                case Keys.F2:
                    this.Price1TextBox.Focus();
                    this.Price1TextBox.SelectAll();
                    return true; // signal that we've processed this key

                case Keys.F3:
                    this.Price1BackTextBox2.Focus();
                    this.Price1BackTextBox2.SelectAll();
                    return true; // signal that we've processed this key

                case Keys.F12:
                    var lottonum = this.LottoTextBox1.Text;
                    var Price = this.Price1TextBox.Text.ToDecimal();
                    var PriceFactor = this.Price1BackTextBox2.Text.ToDecimal();

                    AddLottoNumber(lottonum, Price, PriceFactor);
                    return true;
            }

            // run base implementation
            return base.ProcessCmdKey(ref message, keys);
        }


        #region Reload
        public void Reload()
        {
            try
            {
                this.Enabled = false;
                this.Cursor = Cursors.WaitCursor;

                if (_Istartup)
                {
                    _DATASHOWMASTER.Columns.Add("LottoNumber", typeof(string));
                    _DATASHOWMASTER.Columns.Add("Price1", typeof(decimal));
                    _DATASHOWMASTER.Columns.Add("Price2", typeof(decimal));
                    _DATASHOWMASTER.Columns.Add("Amount", typeof(decimal));
                    _DATASHOWMASTER.Columns.Add("UpdateWhen", typeof(DateTime));
                    for (int i = 0; i < 1000; i++)
                    {
                        var lottonumber = string.Format("{0:000}", i);

                        _DATASHOWMASTER.Rows.Add(lottonumber, 0, 0, 0, null);
                    }

                    _dtDisplay = _DATASHOWMASTER.Copy();
                    int width = 350;
                    this.bindingSource1.DataSource = _dtDisplay;
                    this.dataGridViewShow.DataSource = this.bindingSource1;

                    this.dataGridViewShow.Columns["LottoNumber"].HeaderText = "ตัวเลข";
                    this.dataGridViewShow.Columns["LottoNumber"].Width = width;

                    this.dataGridViewShow.Columns["Price1"].Width = width;
                    this.dataGridViewShow.Columns["Price1"].HeaderText = "เงินรับไว้";
                    this.dataGridViewShow.Columns["Price1"].DefaultCellStyle.Format = "N0";
                    this.dataGridViewShow.Columns["Price1"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
                    this.dataGridViewShow.Columns["Price1"].DefaultCellStyle.BackColor = Color.FromArgb(192, 255, 192);


                    this.dataGridViewShow.Columns["Price2"].HeaderText = "เงินส่งออก";
                    this.dataGridViewShow.Columns["Price2"].DefaultCellStyle.Format = "N0";
                    this.dataGridViewShow.Columns["Price2"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
                    this.dataGridViewShow.Columns["Price2"].Width = width;
                    this.dataGridViewShow.Columns["Price2"].DefaultCellStyle.BackColor = Color.FromArgb(255, 192, 192);


                    this.dataGridViewShow.Columns["Amount"].HeaderText = "จำนวนรวม";
                    this.dataGridViewShow.Columns["Amount"].DefaultCellStyle.Format = "N0";
                    this.dataGridViewShow.Columns["Amount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
                    this.dataGridViewShow.Columns["Amount"].Width = width;
                    this.dataGridViewShow.Columns["Amount"].DefaultCellStyle.BackColor = Color.FromArgb(192, 255, 255);

                    
                    this.dataGridViewShow.Columns["UpdateWhen"].DefaultCellStyle.Format = "dd/MM/yy HH:mm";
                    this.dataGridViewShow.Columns["UpdateWhen"].HeaderText = "บันทึกเมื่อ";
                    this.dataGridViewShow.Columns["UpdateWhen"].Visible = false;


                    _Istartup = false;
                }
                

                //var str = string.Format("SELECT * FROM LottoLimited WHERE LottoType = '{0}' ", _LottoType);
                //table.Source = Base.Class.TableEntity.Database.LigorLotto;
                //if (table.SQLQuery(str))
                //{
                //    _dtLimited = table.Records;
                //    if (_dtLimited.Rows.Count > 0)
                //    {
                //        _CurrentLimited = (decimal)_dtLimited.Rows[0]["MaxPrice"];
                //        Console.WriteLine("MaxPrice = {0:N0}", _CurrentLimited);
                //        this.label6.Text = string.Format("จำนวนเงินอั้น: {0:N0} บาท", _CurrentLimited);
                //    }
                //}

                using (LOTTOEntities lotto = new LOTTOEntities())
                {
                    var query = (from db in lotto.LottoLimited
                                where db.LottoType == _LottoType
                                select db).FirstOrDefault();

                    _CurrentLimited = query.MaxPrice.Value;
                }

                Console.WriteLine("MaxPrice = {0:N0}", _CurrentLimited);
                this.label6.Text = string.Format("จำนวนเงินอั้น: {0:N0} บาท", _CurrentLimited);

                //for (int i = 0; i < 1000; i++)
                //{
                //    var lotto = string.Format("{0:000}", i);
                //    var price = 90m;
                //    Console.WriteLine("{0}={1}", lotto, price);
                //    AddLottoNumber(lotto, price, 0);
                //}

                Relink();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                this.Enabled = true;
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region Relink
        public class LottoItem
        {
            public string LottoNumber { get; set; }
            public decimal Price1 { get; set; }
            public decimal Price2 { get; set; }
            public decimal Amount { get; set; }
        }
        private void Relink()
        {
            try
            {
                // show data
                this.Cursor = Cursors.AppStarting;

                //foreach (DataRow row in _dtDisplay.Rows)
                //{
                //    row["Price1"] = 0;
                //    row["Price2"] = 0;
                //    row["Amount"] = 0;
                //    row["UpdateWhen"] = DBNull.Value;
                //}
                //_dtDisplay = _DATASHOWMASTER.Clone();
                
                _dtDisplay = _DATASHOWMASTER.Copy();
                var ListLotto = new List<LottoItem>();
                using (LOTTOEntities lotto = new LOTTOEntities())
                {
                    var query = (from db in lotto.LottoNumber
                                 where db.LottoType == _LottoType && DbFunctions.TruncateTime(db.LottoDate) == _CurrentDate
                                 group db by new { db.LottoNumber1 } into g
                                 select new { LottoNumber = g.Key.LottoNumber1,
                                              Price1 = g.Select(x=> x.Price1).Sum(),
                                              Price2 = g.Select(x=> x.Price2).Sum(),
                                              Amount = g.Select(x => x.Amount).Sum(),
                                 }
                                 ).Select(x=> new LottoItem()
                                 {
                                     LottoNumber = x.LottoNumber,
                                     Price1 = x.Price1.Value,
                                     Price2 = x.Price2.Value,
                                     Amount = x.Amount.Value
                                 });


                    ListLotto = query.ToList<LottoItem>();

                }

                bool isClear = true;
                foreach (var rowCurrent in ListLotto)
                {
                    var lottonumber = rowCurrent.LottoNumber;
                    var Price1 = rowCurrent.Price1;
                    var Price2 = rowCurrent.Price2;
                    var Amount = rowCurrent.Amount;
                    
                    var rowupdate = _dtDisplay.AsEnumerable()
                                    .Where(row => row.Field<string>("LottoNumber") == lottonumber)
                                    .First(); // not possible with DataTable.Select
                    if (rowupdate != null)
                    {
                        decimal tempPrice1 = 0;
                        decimal tempPrice2 = 0;
                        decimal tempAmount = 0;

                        if (!isClear)
                        {
                            tempPrice1 = (decimal)rowupdate["Price1"];
                            tempPrice2 = (decimal)rowupdate["Price2"];
                            tempAmount = (decimal)rowupdate["Amount"];
                            
                        }

                        rowupdate["Price1"] = tempPrice1 + Price1;
                        rowupdate["Price2"] = tempPrice2 + Price2;
                        rowupdate["Amount"] = tempAmount + Amount;

                        isClear = false;
                    }
                }

                this.bindingSource1.DataSource = _dtDisplay;

                //var strLottoNumber = string.Format("SELECT * FROM LottoNumber WHERE LottoType = '{0}' AND LottoDate = #{1}# ORDER BY PrintSequenceNumber ASC", _LottoType, _CurrentDate.ToString("yyyy-MM-dd", new CultureInfo("en-US")));
                //table.Source = Base.Class.TableEntity.Database.LigorLotto;
                //if (table.SQLQuery(strLottoNumber))
                //{
                //    if (table.Records.Rows.Count > 0)
                //    {
                //        bool isClear = true;
                //        foreach (DataRow rowCurrent in table.Records.Rows)
                //        {
                //            var lottonumber = rowCurrent["LottoNumber"].ToString();
                //            var Price1 = rowCurrent["Price1"] == DBNull.Value ? 0 : (decimal)rowCurrent["Price1"];
                //            var Price2 = rowCurrent["Price2"] == DBNull.Value ? 0 : (decimal)rowCurrent["Price2"];
                //            var Amount = rowCurrent["Amount"] == DBNull.Value ? 0 : (decimal)rowCurrent["Amount"];

                //            var rowupdate = _dtDisplay.Select(string.Format("LottoNumber = '{0}'", lottonumber));
                //            if (rowupdate.Length > 0)
                //            {
                //                decimal tempPrice1 = 0;
                //                decimal tempPrice2 = 0;
                //                decimal tempAmount = 0;

                //                if (!isClear)
                //                {
                //                     tempPrice1 = (decimal)rowupdate[0]["Price1"];
                //                     tempPrice2 = (decimal)rowupdate[0]["Price2"];
                //                     tempAmount = (decimal)rowupdate[0]["Amount"];
                //                }

                //                rowupdate[0]["Price1"] = tempPrice1 + Price1;
                //                rowupdate[0]["Price2"] = tempPrice2 + Price2;
                //                rowupdate[0]["Amount"] = tempAmount + Amount;
                //                rowupdate[0]["UpdateWhen"] = rowCurrent["UpdateWhen"];
                //                isClear = false;
                //            }
                //        }
                //    }
                //}

                GetTotalPrice();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        #endregion
        
        #region Add LottoNumber
        private void AddLottoNumber(string lottonum = "",decimal Price = 0,decimal PriceFactor = 0)
        {
            var IsSuccess = false;
            try
            {
                this.btAddLotto.Enabled = false;
                this.Cursor = Cursors.AppStarting;

               

                if(PriceFactor != 0 && Price == 0)
                {
                    Price = PriceFactor;
                }
                var ListLottoNumber = new List<LottoNumber>();
                if (lottonum.Length != 3 || Price == 0 )
                {
                    return;
                }

                ListLottoNumber.Add(new LottoNumber() { lotto = lottonum, price = Price });
                if (Math.Abs(PriceFactor) > 0)
                {
                    // กลับตัวเลข

                    var lottonumbers = lottonum.ToArray();
                    var tempList = new List<string>();

                    tempList.Add(string.Format("{0}{1}{2}", lottonumbers[0], lottonumbers[2], lottonumbers[1]));
                    tempList.Add(string.Format("{0}{1}{2}", lottonumbers[1], lottonumbers[0], lottonumbers[2]));
                    tempList.Add(string.Format("{0}{1}{2}", lottonumbers[1], lottonumbers[2], lottonumbers[0]));
                    tempList.Add(string.Format("{0}{1}{2}", lottonumbers[2], lottonumbers[1], lottonumbers[0]));
                    tempList.Add(string.Format("{0}{1}{2}", lottonumbers[2], lottonumbers[0], lottonumbers[1]));

                    foreach (var item in tempList)
                    {
                        try
                        {
                            var result = ListLottoNumber.Find(x => x.lotto == item);
                            if (result.lotto == null)
                            {
                                ListLottoNumber.Add(new LottoNumber() { lotto = item, price = PriceFactor });
                            }
                        }
                        catch (ArgumentNullException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        catch(Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                       
                    }
                }

                var MessageText = string.Empty;
                foreach (var item in ListLottoNumber)
                {
                    MessageText += (MessageText.Length == 0 ? "" : ", ") + string.Format("{0}={1:N0}", item.lotto, item.price);
                    bool IsAdd = false;
                    var RealPrice = item.price;
                    var PrintSequenceNumber = -1;
                    decimal OriginalTotalPrice = 0;
                    decimal OriginalPrice1All = 0;
                    decimal OriginalPrice2All = 0;

                    var ListLotto = new List<LottoMachine.LottoNumber>();
                    using (LOTTOEntities lotto = new LOTTOEntities())
                    {
                        var query = (from db in lotto.LottoNumber
                                     where db.LottoType == _LottoType && DbFunctions.TruncateTime(db.LottoDate) == _CurrentDate && db.LottoNumber1 == item.lotto
                                     orderby db.PrintSequenceNumber
                                     select db);

                        ListLotto = query.ToList();
                        if (query == null || query.Count() == 0)
                        {
                            IsAdd = true;
                        }
                        else
                        {
                            foreach (var Litem in ListLotto)
                            {
                                OriginalPrice1All = Litem.Price1 == null ? 0 : (decimal)Litem.Price1;
                                OriginalPrice2All = Litem.Price2 == null ? 0 : (decimal)Litem.Price2;
                                OriginalTotalPrice += OriginalPrice1All + OriginalPrice2All;
                            }
                            PrintSequenceNumber = ListLotto[0].PrintSequenceNumber.Value;
                            if (PrintSequenceNumber > 0)
                            {
                                IsAdd = true;
                            }
                        }

                    }
                    //var str = string.Format("SELECT * FROM LottoNumber WHERE LottoType = '{0}' AND LottoDate = #{1}# AND LottoNumber = '{2}' ORDER BY PrintSequenceNumber ASC", _LottoType, _CurrentDate.ToString("yyyy-MM-dd", new CultureInfo("en-US")), item.lotto);
                    //table.Source = Base.Class.TableEntity.Database.LigorLotto;
                    //if (table.SQLQuery(str))
                    //{
                    //    if (table.Records.Rows.Count == 0) IsAdd = true;
                    //    else
                    //    {
                    //        foreach (DataRow row in table.Records.Rows)
                    //        {
                    //            OriginalPrice1All   = row["Price1"] == DBNull.Value ? 0 : (decimal)row["Price1"];
                    //            OriginalPrice2All   = row["Price2"] == DBNull.Value ? 0 : (decimal)row["Price2"];
                    //            OriginalTotalPrice += OriginalPrice1All + OriginalPrice2All;
                    //        }
                    //        PrintSequenceNumber = (int)table.Records.Rows[0]["PrintSequenceNumber"];
                    //        if (PrintSequenceNumber > 0)
                    //        {
                    //            IsAdd = true;
                    //        }
                                
                    //    }
                    //}
                    decimal Price1 = 0;
                    decimal Price2 = 0;
                    decimal Amount = 0;
                    decimal Price1Show = 0;
                    decimal Price2Show = 0;
                    if (IsAdd)
                    {
                        if (OriginalTotalPrice == 0)
                        {
                            // ต้อง GET จำนวน ล่าสุดก่อน เพิ่ม
                            Price1 = RealPrice > _CurrentLimited ? _CurrentLimited : RealPrice;
                            Price2 = RealPrice > _CurrentLimited ? RealPrice - _CurrentLimited : 0;
                            Amount = Price1 + Price2;
                        }
                        else
                        {
                            // เลขเต็มแล้ว
                            if (OriginalTotalPrice >= _CurrentLimited)
                            {
                                Price2 = RealPrice;
                                Amount = Price2;
                            }
                            else
                            {
                                var tempLimited = _CurrentLimited - OriginalTotalPrice;
                                Price1 = RealPrice > tempLimited ? tempLimited : RealPrice;
                                Price2 = RealPrice > tempLimited ? RealPrice - tempLimited : 0;
                                Amount = Price1 + Price2;
                            }
                        }



                        if (Price1 < 0) Price1 = 0;
                        if (Price2 < 0) Price2 = 0;
                        if (Amount < 0) Amount = 0;

                        if(Price1 >  0 || Price2 > 0)
                        {
                            // this.lottoNumberTableAdapter1.Insert(_LottoType,
                            //_CurrentDate,
                            //item.lotto,
                            //Price1,
                            //Price2,
                            //Amount, 0, null, null, DateTime.Now
                            //);
                            var newLotto = new LottoMachine.LottoNumber();
                            newLotto.LottoDate = _CurrentDate.Date;
                            newLotto.LottoType = _LottoType;
                            newLotto.LottoNumber1 = item.lotto;
                            newLotto.Price1 = Price1;
                            newLotto.Price2 = Price2;
                            newLotto.Amount = Amount;
                            newLotto.PrintSequenceNumber = 0;
                            newLotto.UpdateWhen = DateTime.Now;

                            using (LOTTOEntities lotto = new LOTTOEntities())
                            {
                                var result = lotto.LottoNumber.Add(newLotto);
                                var add = lotto.SaveChanges();
                                IsSuccess = add > 0;
                            }
                            
                        }
                    }
                    else
                    {
                        // Update
                        //var rowUpdate = table.Records.Rows[0];
                        //var OriginalPrice = (decimal)rowUpdate["Price1"];
                        //var OriginalPrice2 = (decimal)rowUpdate["Price2"];

                        var rowUpdate = ListLotto[0];
                        var OriginalPrice   = rowUpdate.Price1 != null ? rowUpdate.Price1.Value : 0 ;
                        var OriginalPrice2  = rowUpdate.Price2 != null ? rowUpdate.Price2.Value : 0 ;
                        Price1 = OriginalPrice;
                        Price2 = OriginalPrice2;
                        Amount = 0;
                        

                        if (OriginalTotalPrice == 0)
                        {
                            if (OriginalPrice >= _CurrentLimited)
                            {
                                Price2 += RealPrice;
                            }
                            else
                            {
                                if (OriginalPrice + RealPrice >= _CurrentLimited)
                                {
                                    Price1 = _CurrentLimited;
                                    Price2 = RealPrice - (_CurrentLimited - OriginalPrice);
                                }
                                else
                                {
                                    Price1 = OriginalPrice + RealPrice;
                                }
                            }
                        }
                        else
                        {
                            // เลขเต็มแล้ว
                            if (OriginalTotalPrice >= _CurrentLimited)
                            {
                                Price2 += RealPrice;
                                Amount = Price2;
                            }
                            else
                            {
                                // เลขยังไม่เต็ม
                                //var valueTEmpty = _CurrentLimited - OriginalPrice;
                                //if(REalti)
                                var ValueEmpty = _CurrentLimited - OriginalTotalPrice;
                                if(ValueEmpty == 0)
                                {
                                    Price2 += RealPrice;
                                }
                                else
                                {
                                    if (OriginalPrice + RealPrice >= _CurrentLimited)
                                    {
                                        Price1 = _CurrentLimited;
                                        Price2 = RealPrice - (_CurrentLimited - OriginalPrice);
                                    }
                                    else
                                    {
                                        Price1 = OriginalPrice + RealPrice;
                                    }

                                }


                                //if (OriginalPrice >= ValueEmpty)
                                //{
                                //    Price2 += RealPrice;
                                //}
                                //else
                                //{
                                //    if (OriginalPrice + RealPrice >= ValueEmpty)
                                //    {
                                //        Price1 = _CurrentLimited;
                                //        Price2 = RealPrice - (_CurrentLimited - OriginalPrice);
                                //    }
                                //    else
                                //    {
                                //        Price1 = OriginalPrice + RealPrice;
                                //    }
                                //}
                            }
                        }


                        Amount = Price1 + Price2;

                        if (Price1 < 0) Price1 = 0;
                        if (Price2 < 0)
                        {
                            if(Price1 > 0)
                            {
                                Price1 += Price2;
                                if (Price1 < 0)
                                    Price1 = 0;
                            }
                            Price2 = 0;
                        }
                        if (Amount < 0) Amount = 0;

                        //rowUpdate["Price1"] = Price1;
                        //rowUpdate["Price2"] = Price2;
                        //rowUpdate["Amount"] = Amount;
                        //rowUpdate["UpdateUser"] = Global.CurrentUsername;
                        //rowUpdate["UpdateWhen"] = DateTime.Now;
                        //// Update
                        //int UpdateRows = this.lottoNumberTableAdapter1.Update(rowUpdate);
                        //Console.WriteLine("UpdateRows = {0}", UpdateRows);
                        //if (UpdateRows > 0)
                        //    IsSuccess = true;

                        rowUpdate.Price1 = Price1;
                        rowUpdate.Price2 = Price2;
                        rowUpdate.Amount = Amount;
                        rowUpdate.UpdateWhen = DateTime.Now;

                        using (LOTTOEntities lotto = new LOTTOEntities())
                        {
                            var entry = lotto.Entry(rowUpdate);
                            entry.State = EntityState.Modified;

                            int UpdateRow = lotto.SaveChanges();
                            Console.WriteLine("UpdateRows = {0}", UpdateRow);
                            if (UpdateRow > 0)
                                IsSuccess = true;
                        }

                    }

                    //var rowdisplay = _dtDisplay.Select(string.Format("LottoNumber = '{0}'", item.lotto));
                    //if (rowdisplay.Length > 0)
                    //{
                    //    rowdisplay[0]["Price1"] = Price1;
                    //    rowdisplay[0]["Price2"] = OriginalPrice2All + Price2;
                    //    rowdisplay[0]["Amount"] = Price1 + OriginalPrice2All + Price2;
                    //    rowdisplay[0]["UpdateWhen"] = DateTime.Now;
                    //}
                    // Display
                }

                if(!string.IsNullOrEmpty(MessageText) )
                {
                    var textMessage = string.Format("{0:HH:mm:ss} : {1}",DateTime.Now,MessageText);
                    //if(IsSuccess)
                    //{
                    //    textMessage += " ทำรายการไม่สำเร็จ!";
                    //}

                    ShowMessage(MessageText,Color.Green);
                }

                Relink();

                this.LottoTextBox1.Clear();
                this.Price1BackTextBox2.Clear();
                this.Price1TextBox.Clear();
                this.LottoTextBox1.Focus();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {

                GetTotalPrice();

                this.btAddLotto.Enabled = true;
                this.Cursor = Cursors.Default;

            }
        }
        private void GetTotalPrice()
        {
            try
            {
                _CurrentTotalAll = 0;
                _CurrentTotalPrice1 = 0;
                _CurrentTotalPrice2 = 0;
                // On local
                if(_dtDisplay.Rows.Count > 0)
                {
                   _CurrentTotalPrice1 =  _dtDisplay.AsEnumerable().Sum(x => x.Field<decimal>("Price1"));
                   _CurrentTotalPrice2 = _dtDisplay.AsEnumerable().Sum(x => x.Field<decimal>("Price2"));
                   _CurrentTotalAll    = _CurrentTotalPrice1 + _CurrentTotalPrice2;
                }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                
            }
            finally
            {
                this.textBox4.Text = _CurrentTotalPrice1.ToString("N0");
                this.textBox1.Text = _CurrentTotalPrice2.ToString("N0");
                this.textBox2.Text = _CurrentTotalAll.ToString("N0");
            }
        }
        private void btAddLotto_Click(object sender, EventArgs e)
        {
            var lottonum = this.LottoTextBox1.Text;
            var Price = this.Price1TextBox.Text.ToDecimal();
            var PriceFactor = this.Price1BackTextBox2.Text.ToDecimal();

            AddLottoNumber(lottonum, Price, PriceFactor);
        }
        
        private void LottoTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (Keys.Enter == e.KeyCode)
                {
                    bool isNext = true;
                    var tb = sender as TextBox;
                    var tag = tb.Tag;
                    if (tag != null)
                    {
                        if (tb.TextLength < 3)
                            isNext = false;
                    }
                    //else
                    //{
                    //    if (tb.TextLength == 0)
                    //        isNext = false;
                    //}

                    if (isNext)
                    {
                        Console.Beep(888, 20);
                        SendKeys.Send("{TAB}");
                        e.Handled = true;//set to false if you need that textbox gets enter key
                        e.SuppressKeyPress = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
           
        }
        private void LottoTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }

            //// only allow one decimal point
            //if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            //{
            //    e.Handled = true;
            //}
        }
        private void Price1BackTextBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keys.Enter == e.KeyCode)
            {
                //e.Handled = true;
                if (this.Price1TextBox.TextLength > 0 ||  this.Price1BackTextBox2.TextLength > 0)
                {
                    Console.Beep(888, 20);
                    var lottonum = this.LottoTextBox1.Text;
                    var Price = this.Price1TextBox.Text.ToDecimal();
                    var PriceFactor = this.Price1BackTextBox2.Text.ToDecimal();
                   
                    AddLottoNumber(lottonum, Price, PriceFactor);
                }
                else
                {
                    Console.Beep(888, 20);
                    SendKeys.Send("{TAB}");
                    e.Handled = true;//set to false if you need that textbox gets enter key
                    e.SuppressKeyPress = true;
                }

            }
        }

        #endregion

        #region Events
        private void ShowAllCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            this.bindingSource1.Filter = this.ShowAllCheckbox.Checked ? "Price1 > 0 " : ""; 
        }
      
        private void button1_Click(object sender, EventArgs e)
        {
            Relink();
        }

        #endregion

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                // set the current caret position to the end
                //this.richTextBox1.SelectionStart = richTextBox1.Text.Length;
                // scroll it automatically
                //this.richTextBox1.ScrollToCaret();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            
        }
        public void ShowMessage(string text, Color color)
        {
            try
            {
                //string message = string.Empty;
                //var DateTextMessage = "[" + DateTime.Now.ToString("dd/MM/yy HH:mm:ss") + "] : ";
                //richTextBox1.Invoke(new Action(() =>
                //{

                //    this.richTextBox1.SelectedText = DateTextMessage;
                //    this.richTextBox1.SelectionColor = color;
                //    this.richTextBox1.SelectedText = text + Environment.NewLine;
                //    message = DateTextMessage + text + Environment.NewLine;
                //    if (richTextBox1.TextLength > 200000)
                //        richTextBox1.Clear();
                //}));



            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }
    }
}
