﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LottoMachine
{
    public partial class SelectLottoDateForm : Form
    {
        public SelectLottoDateForm()
        {
            InitializeComponent();
        }
        private void Reload()
        {
            try
            {
                this.dateTimePicker1.MinDate = DateTime.Now.Date;
                this.dateTimePicker1.MaxDate = DateTime.Now.Date.AddDays(30);

                if(DateTime.Now.Date.Day == 1 || DateTime.Now.Date.Day == 16)
                {
                    return;
                }
                if (DateTime.Now.Date.Day > 1 && DateTime.Now.Date.Day < 16)
                {
                    this.dateTimePicker1.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 16);
                }
                else
                {
                    // Over 16
                    var nextmonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1);
                    this.dateTimePicker1.Value = nextmonth;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void Relink()
        {

            this.label2.Text = this.dateTimePicker1.Value.ToString("dd");
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            Relink();
        }

        private void btConfrim_Click(object sender, EventArgs e)
        {
            Global.CurrentLottoDate = this.dateTimePicker1.Value.Date;
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void SelectLottoDateForm_Load(object sender, EventArgs e)
        {
            Reload();
        }
    }
}
