﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LottoMachine
{
    public partial class ActivationForm : Form
    {
        private string _ProductKeyRealTime = "";
        
        Privacy.License lc = new Privacy.License();

        public string ProductKeyRealTime { get { return _ProductKeyRealTime; } set { _ProductKeyRealTime = value; } }
        public string hdID { set { this.textBox1.Text = value; } }
        public string ProductKey { get { return textBox2.Text; } }

        public string TextShow { get { return this.label3.Text; } set { this.label3.Text = value; } }
        public ActivationForm()
        {
            InitializeComponent();
        }

        private void SubmitVersion1()
        {
            try
            {
                var MachineCode = this.textBox1.Text;
                var key = this.textBox2.Text;
                if (string.IsNullOrEmpty(key))
                {
                    MessageBox.Show("กรุณาใส่คีย์เพื่อทำการเปิดใช้งานโปรแกรมด้วยค่ะ");
                    return;
                }
                if (lc.MakeKey(MachineCode, key))
                {
                    MessageBox.Show("ขอบคุณที่ลงทะเบียนผลิตภัณฑ์", "ลงทะเบียน", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = System.Windows.Forms.DialogResult.Yes;

                }
                else
                {
                    MessageBox.Show("หมายเลขผลิตภัณฑ์ไม่ถูกต้อง กรุณาลองอีกครั้งค่ะ", "ลงทะเบียน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void btok_Click(object sender, EventArgs e)
        {
            try
            {
                var MachineCode = this.textBox1.Text;
                var key         = this.textBox2.Text;
                if(string.IsNullOrEmpty(key))
                {
                    MessageBox.Show("กรุณาใส่คีย์เพื่อทำการเปิดใช้งานโปรแกรมด้วยค่ะ");
                    return;
                }


                if (ProductKeyRealTime.Equals(key))
                {
                    MessageBox.Show("ขอบคุณที่ลงทะเบียนผลิตภัณฑ์", "ลงทะเบียน", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = System.Windows.Forms.DialogResult.Yes;

                }
                else
                {
                    MessageBox.Show("หมายเลขผลิตภัณฑ์ไม่ถูกต้อง กรุณาลองอีกครั้งค่ะ", "ลงทะเบียน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
           

        }
    }
}
