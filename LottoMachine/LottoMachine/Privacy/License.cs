﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace LottoMachine.Privacy
{
    public class License
    {
        const string userRoot = "HKEY_CURRENT_USER\\Software\\Ligor";
        const string HardwareKey = "HardwareKey";
        const string ProductKey = "ProductKey";
        const string keyNameHardwareKey = userRoot + "\\" + HardwareKey;
        const string keyNameProductKey = userRoot + "\\" + ProductKey;

        private const string PRODUCTKEY = "PRODUCTKEY";
        private const string password = "sdgfkjro0tjaolsdfkldfasjgoiajglka1s156dfs15f5sd1f";

        ModifyRegistry reg = new ModifyRegistry();

        public License()
        {

        }
        private string keygen() // gets the random string  
        {
            string pkey;
            // Create application runs file if it not exists (one time)
            pkey = RandomString() + RandomInt();
            return pkey;
        }

        private string RandomString() // gets the first part of random string
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < 8; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            return builder.ToString();
        }

        private string RandomInt() // gets the second part of random string
        {
            int n;
            string ret = "";
            Random r = new Random();

            n = r.Next(11111, 99999);
            ret = n.ToString();

            return ret;
        }


        public string GetKey(string ID)
        {
            String pkey = ID;//keygen();
            char[] key1 = new char[19];

            key1[0] = GetChar(pkey[8]);
            key1[1] = GetNum(pkey[4]);
            key1[2] = GetNum(pkey[1]);
            key1[3] = '9';
            key1[4] = '-';
            key1[5] = GetChar(pkey[9]);
            key1[6] = '9';
            key1[7] = GetNum(pkey[2]);
            key1[8] = GetChar(pkey[10]);
            key1[9] = '-';
            key1[10] = GetChar(pkey[12]);
            key1[11] = GetNum(pkey[3]);
            key1[12] = GetNum(pkey[6]);
            key1[13] = '9';
            key1[14] = '-';
            key1[15] = GetChar(pkey[11]);
            key1[16] = GetNum(pkey[0]);
            key1[17] = GetNum(pkey[7]);
            key1[18] = GetNum(pkey[5]);

            string skey = "";
            int i;

            // 999 application runs by default 
            // To change, edit key[3], key[6] & key[13] which denotes 3 digits of runs respectively (max : 999)

            for (i = 0; i < 19; i++)
                skey += key1[i];

            return skey;
        }
        public bool MakeKey(string ID,string productKey) // Generating the topup key
        {
            if (GetKey(ID) == productKey) 
                return true;
            else return false;
            //return skey;
        }

        // key generation
        // edit as per your wish (make sure that you will make same changes on both the system components)

        private char GetChar(char num) //gets the char for every numner passed (coding first part of the random string)
        {
            char ret = ' ';

            if (num == '1')
                ret = 'O';
            if (num == '2')
                ret = 'W';
            if (num == '3')
                ret = 'U';
            if (num == '4')
                ret = 'R';
            if (num == '5')
                ret = 'V';
            if (num == '6')
                ret = 'X';
            if (num == '7')
                ret = 'S';
            if (num == '8')
                ret = 'T';
            if (num == '9')
                ret = 'N';
            if (num == '0')
                ret = 'Z';

            return ret;
        }

        private char GetNum(char str) // gets the numbers for every char passed (coding second part of the random string)
        {
            char ret = ' ';

            if (str == 'A')
                ret = '2';
            if (str == 'B')
                ret = '6';
            if (str == 'C')
                ret = '7';
            if (str == 'D')
                ret = '2';
            if (str == 'E')
                ret = '1';
            if (str == 'F')
                ret = '7';
            if (str == 'G')
                ret = '8';
            if (str == 'H')
                ret = '9';
            if (str == 'I')
                ret = '4';
            if (str == 'J')
                ret = '4';
            if (str == 'K')
                ret = '3';
            if (str == 'L')
                ret = '9';
            if (str == 'M')
                ret = '9';
            if (str == 'N')
                ret = '3';
            if (str == 'O')
                ret = '4';
            if (str == 'P')
                ret = '9';
            if (str == 'Q')
                ret = '7';
            if (str == 'R')
                ret = '6';
            if (str == 'S')
                ret = '9';
            if (str == 'T')
                ret = '9';
            if (str == 'U')
                ret = '9';
            if (str == 'V')
                ret = '5';
            if (str == 'W')
                ret = '4';
            if (str == 'X')
                ret = '2';
            if (str == 'Y')
                ret = '8';
            if (str == 'Z')
                ret = '2';

            return ret;
        }
        //public bool CheckActivation()
        //{
        //    string pgkey = reg.Read(ProductKey);
        //    string pgHD = reg.Read(HardwareKey);
        //    string OriginalKeyMachine = keygen();

            

        //    if (pgkey == null)
        //    {
        //        string key = pgHD;
        //        if (key == null)
        //        {
        //           key =  keygen();

        //           reg.Write(HardwareKey, key);
        //        }
        //        ActivationForm dlg = new ActivationForm();
        //        dlg.hdID = key;
        //        if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.Yes)
        //        {
        //            reg.Write(ProductKey, dlg.ProductKey);
        //            return true;
        //        }
        //        return false;
        //    }
        //    else 
        //    {
        //        if (pgkey == GetKey(pgHD))
        //            return true;
        //        else
        //            return false; // รหัสผิดพาด
        //    }
        //}

        public bool CheckActivation()
        {
            bool isSuccess = false;

            try
            {
                var SerialNumber = Privacy.Cipher.identifier("Win32_DiskDrive", "SerialNumber");
                var ProductKeyOnRealTime = Privacy.Cipher.Encrypt(SerialNumber, password);

                
                var ProductKeyOnRegistry = reg.Read(PRODUCTKEY);
                if (ProductKeyOnRegistry == null)
                {
                    reg.Write(HardwareKey, "ERP is not activate!");
                    ProductKeyOnRegistry = "";
                }

                Console.WriteLine("KEY REALTIME : {0}", ProductKeyOnRealTime);
                Console.WriteLine("KEY REGISTY  : {0}", ProductKeyOnRegistry);

                if (string.IsNullOrEmpty(ProductKeyOnRealTime))
                {
                    // Canot Get SerailNUmber
                    return false;
                }

                var TextShow = "";
                if(!ProductKeyOnRealTime.Equals(ProductKeyOnRegistry))
                {
                    TextShow = "รหัสผลิตภัณฑ์ไม่ถูกต้อง กรุณาติดต่อผู้พัฒนาโปรแกรม!";
                }
                if(string.IsNullOrEmpty(ProductKeyOnRegistry))
                {
                    TextShow = "คุณยังไม่ได้ลงทะเบียนผลิตภัณฑ์ กรุณากรอกหมายเลขคีย์เพื่อใช้งานโปรแกรม";
                }
                if (string.IsNullOrEmpty(ProductKeyOnRegistry) || !ProductKeyOnRealTime.Equals(ProductKeyOnRegistry))
                {
                    var dlg = new ActivationForm();
                    dlg.hdID = SerialNumber;
                    dlg.ProductKeyRealTime = ProductKeyOnRealTime;
                    dlg.TextShow = TextShow;
                    if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.Yes)
                    {
                        reg.Write(PRODUCTKEY, ProductKeyOnRealTime);
                        isSuccess = true;
                    }
                }
                else
                {
                    if (ProductKeyOnRealTime == ProductKeyOnRegistry)
                    {
                        isSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            


            return isSuccess;

            
        }
    }
}
