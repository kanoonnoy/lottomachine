﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LottoMachine
{
    public static class StringExtension
    {
        public static double ToDouble(this string s)
        {
            var newvalue = 0d;
            try
            {
                newvalue = Convert.ToDouble(s);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return newvalue;
        }
        public static decimal ToDecimal(this string s)
        {
            decimal newvalue = 0;
            try
            {
                newvalue = Convert.ToDecimal(s);
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
            }
            return newvalue;
        }
    }
}
