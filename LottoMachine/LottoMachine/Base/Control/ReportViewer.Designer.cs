﻿namespace LottoMachine.Base.Control
{
    partial class ReportViewer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.crystalViewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // crystalViewer
            // 
            this.crystalViewer.ActiveViewIndex = -1;
            this.crystalViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalViewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalViewer.DisplayBackgroundEdge = false;
            this.crystalViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalViewer.Location = new System.Drawing.Point(0, 0);
            this.crystalViewer.Margin = new System.Windows.Forms.Padding(4);
            this.crystalViewer.Name = "crystalViewer";
            this.crystalViewer.ShowCloseButton = false;
            this.crystalViewer.ShowCopyButton = false;
            this.crystalViewer.ShowGroupTreeButton = false;
            this.crystalViewer.ShowLogo = false;
            this.crystalViewer.ShowParameterPanelButton = false;
            this.crystalViewer.ShowTextSearchButton = false;
            this.crystalViewer.Size = new System.Drawing.Size(531, 458);
            this.crystalViewer.TabIndex = 0;
            this.crystalViewer.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            this.crystalViewer.ToolPanelWidth = 150;
            this.crystalViewer.ReportRefresh += new CrystalDecisions.Windows.Forms.RefreshEventHandler(this.crystalViewer_ReportRefresh);
            // 
            // ReportViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.crystalViewer);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ReportViewer";
            this.Size = new System.Drawing.Size(531, 458);
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalViewer;
    }
}
