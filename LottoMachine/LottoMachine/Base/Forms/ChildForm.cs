﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LottoMachine.Base.Forms
{
    public partial class ChildForm : Form
    {
        bool isStartup = true;
        public ChildForm()
        {
            InitializeComponent();
        }

        private void ChildForm_SizeChanged(object sender, EventArgs e)
        {
            if(isStartup)
            {
                var f = sender as Form;
                f.ShowIcon = !(f.WindowState == FormWindowState.Maximized);
                isStartup = false;
            }
         
        }
    }
}
