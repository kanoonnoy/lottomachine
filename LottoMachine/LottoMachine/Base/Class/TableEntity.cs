﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace LottoMachine.Base.Class
{
    public class TableEntity
    {
        #region Events 
        public event EventHandler WaitingStart;
        public event EventHandler WaitingDone;
        #endregion
        
        #region Properties

        public Database Source { get; set; }
        public bool IsWorking { get; private set; } // Readonly
        public DataTable Records { get; set; } // Readonly
        public DataTable RecordsforAction { get; set; }
        public string PrimaryKey { get; set; }
        public string PrimarykeyValue { get; set; }
        public string TableName { get; set; }


        #endregion
        
        #region Parameter

        public enum Database { None, LigorLotto };
        private string SelectPathDatabase(Database db, int EntityID = -1)
        {
            string ConnectionString = string.Empty;
            if (db == Database.LigorLotto)
            {
                ConnectionString = Properties.Settings.Default["RealtimeConnectionString"].ToString();
            }
            return ConnectionString;
        }

        #endregion

        #region Constructor

        public TableEntity()
        {
            Source = Database.None; // Set Default Database
        }

        #endregion

        #region Private Methods

        public bool OleQuerySelection(string sql, string path)
        {
            DataSet dataset = new DataSet();
            Records = new DataTable();
            OleDbDataAdapter dataAdp;
            try
            {
                dataAdp = new OleDbDataAdapter(sql, path);
                dataAdp.FillLoadOption = LoadOption.PreserveChanges;
                dataAdp.Fill(dataset);
                Records = dataset.Tables[0];
                dataset.Dispose(); // Releases all resources used
                dataAdp.Dispose(); // Releases all resources used
                return true;
            }
            catch (OleDbException ex)
            {
                Console.WriteLine(ex.Message);
                //MessageBox.Show("Code : " + ex.Message + " \nText : " + ex.Message, "Acess Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;

            }
            catch (Exception ex)
            {
                Console.WriteLine("OLE DataBase error : {0}", ex.Message);
                return false;
            }

        }
        public bool OleNonQuery(string sql, string connection)
        {
            bool success = false;
            using (OleDbConnection myConnection = new OleDbConnection(connection))
            {
                try
                {
                    OleDbTransaction transaction;
                    myConnection.Open();
                    transaction = myConnection.BeginTransaction();
                    OleDbCommand myCommand = new OleDbCommand(sql, myConnection, transaction);
                    if(RecordsforAction != null)
                    {
                        for (int i = 0; i < RecordsforAction.Columns.Count; i++)
                        {
                            myCommand.Parameters.AddWithValue("@" + RecordsforAction.Columns[i].ToString(), RecordsforAction.Rows[0][RecordsforAction.Columns[i]].ToString());
                            Console.WriteLine("Columns = {0}  Value = {1}", RecordsforAction.Columns[i].ToString(), RecordsforAction.Rows[0][RecordsforAction.Columns[i]].ToString());
                        }
                        if (myCommand.CommandText.IndexOf("WHERE") != -1) // Find WHERE and add Condition
                            myCommand.Parameters.AddWithValue("@" + PrimaryKey, PrimarykeyValue);

                    }

                    myCommand.ExecuteNonQuery();
                    transaction.Commit();
                    success = true;
                }
                catch (Exception ex)
                {
                    string Message = ex.Message;
                    success = false;
                    throw ex;

                }
                finally
                {
                    if (myConnection.State == ConnectionState.Open)
                        myConnection.Close();

                }
                if (success)
                    return true;
                else
                    return false;
            }
        }
        public bool SqlQuerySelection(string sql, string connection)
        {
            SqlDataAdapter dtAdapter;
            Records = new DataTable();
            Records.Rows.Clear();
            try
            {
                SqlConnection sqlconnection = new SqlConnection(connection);

                dtAdapter = new SqlDataAdapter(sql, sqlconnection);

                dtAdapter.Fill(Records);
                sqlconnection.Open();
                sqlconnection.Close();
                dtAdapter.Dispose(); // Releases all resources used
                sqlconnection.Dispose();// Releases all resources used

                return true;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            finally
            {

            }
        }
        private bool SQLNoneQuery(string sql, string connection)
        {
            using (SqlConnection db = new SqlConnection(connection))
            {

                int countRow = 0;
                SqlTransaction transaction;
                db.Open();
                transaction = db.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, db, transaction);
                if (RecordsforAction != null)
                {
                    for (int i = 0; i < RecordsforAction.Columns.Count; i++)
                        command.Parameters.AddWithValue("@" + RecordsforAction.Columns[i].ToString(), RecordsforAction.Rows[0][RecordsforAction.Columns[i]].ToString());
                    if (command.CommandText.IndexOf("WHERE") != -1) // Find WHERE and add Condition
                        command.Parameters.AddWithValue("@" + PrimaryKey, PrimarykeyValue);
                }
                // Console.WriteLine("SQL Statement : {0}", sql);
                //Console.WriteLine("SQL Commnd : {0}", command.CommandText);
                try
                {
                    // new SqlCommand(sql, db, transaction).ExecuteNonQuery();
                    countRow = command.ExecuteNonQuery();
                    transaction.Commit();
                    db.Close();
                    db.Dispose();
                    return true;
                }
                catch (SqlException ex)
                {
                    return false;
                }
            }
        }

        #endregion

        #region Public Methods
        public bool WriteToDatabase(string connString, string tableName, DataTable dataTable)
        {
            try
            {
                // get your connection string
                // connect to SQL
                using (SqlConnection connection =
                        new SqlConnection(connString))
                {
                    // make sure to enable triggers
                    // more on triggers in next post
                    SqlBulkCopy bulkCopy =
                        new SqlBulkCopy
                        (
                        connection,
                        SqlBulkCopyOptions.TableLock |
                        SqlBulkCopyOptions.FireTriggers |
                        SqlBulkCopyOptions.UseInternalTransaction,
                        null
                        );

                    // set the destination table name

                    bulkCopy.DestinationTableName = tableName;
                    connection.Open();
                    // write the data in the "dataTable"
                    bulkCopy.WriteToServer(dataTable);
                    connection.Close();

                }
                // reset
                //dataTable.Clear();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
                return false;
            }

        }
        public bool SQLQuery(string Statement, bool waiting = true, int EntityID = -1) /* -1 use default Source */
        {
            // Initial Event
            EventArgs e = new EventArgs();
            bool Success = false;
            IsWorking = true;
            if (this.WaitingStart != null)
                this.WaitingStart(this, e);
            string Path = SelectPathDatabase(Source, EntityID); // Select DataBase
            //Console.WriteLine("Path :---- >  "+Path);
            if (Path.Length == 0)
            {
                Success = false; // Exit method if path not found!
            }
            if (Source == Database.LigorLotto)
            {
                // OLE
                if (OleQuerySelection(Statement, Path))
                    Success = true;
                else
                    Success = false;
            }
            else
            {
                // TODO : Something
                Console.WriteLine("Is not set Source!!!!!!!");
                Success = false;
            }
            // Check Success Query
            if (Success)
            {
                if (this.WaitingDone != null)
                    this.WaitingDone(this, e);
                IsWorking = false;
                return true;
            }
            else
                return false;

        }

        public bool SQLNoneSQLQuery(string Statement, int EntityID = -1)
        {
            string Path = SelectPathDatabase(Source, EntityID);
            return SQLNoneQuery(Statement, Path);
        }
        public bool SQLNoneOleQuery(string Statement, int EntityID = -1)
        {
            string Path = SelectPathDatabase(Source, EntityID);
            return OleNonQuery(Statement, Path);
        }
        public bool ActionInsert()
        {
            if (TableName != "")
            {
                string sql = "INSERT INTO " + TableName + " (";
                if (RecordsforAction.Columns.Count > 0 && RecordsforAction.Rows.Count > 0 && PrimaryKey.Length > 0 && PrimarykeyValue.Length > 0)
                {
                    for (int i = 0; i < RecordsforAction.Columns.Count; i++)
                        sql += RecordsforAction.Columns[i].ToString() + ",";
                    sql = sql.Substring(0, sql.Length - 1) + ") VALUES ("; // delete last ','
                    for (int i = 0; i < RecordsforAction.Columns.Count; i++)
                        sql += "@" + RecordsforAction.Columns[i].ToString() + ",";
                    sql = sql.Substring(0, sql.Length - 1) + ")"; // delete last ','
                    //Console.WriteLine("SQL State : {0}",sql);
                    string path = SelectPathDatabase(Source);
                    return SQLNoneQuery(sql, path);
                }
            }
            return false;
        }
        public bool ActionUpdate()
        {
            if (TableName != "")
            {
                string sql = "UPDATE " + TableName + " SET ";
                if (RecordsforAction.Columns.Count > 0 && RecordsforAction.Rows.Count > 0 && PrimaryKey.Length > 0 && PrimarykeyValue.Length > 0)
                {
                    for (int i = 0; i < RecordsforAction.Columns.Count; i++)
                        sql += RecordsforAction.Columns[i].ToString() + " = @" + RecordsforAction.Columns[i].ToString() + ",";
                    sql = sql.Substring(0, sql.Length - 1); // delete last ','
                    sql += "  WHERE " + PrimaryKey + " = '" + PrimarykeyValue + "'";
                    string path = SelectPathDatabase(Source);
                    //Console.WriteLine(sql);
                    if (Source == Database.LigorLotto)
                        return OleNonQuery(sql, path);
                    else
                        return SQLNoneQuery(sql, path);
                }
            }
            return false; // none use action Update

        }
        public bool ActionDelete()
        {
            return false;
        }

        #endregion

    }
}
